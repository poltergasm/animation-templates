![](demo.gif)

## Why tho?

There's some great animation libraries out there, but I sometimes find myself with spritesheets that have weird quad sizes and a grid-based library just isn't going to cut it. I needed more control, so this one takes in quad dimensions instead of grids, meaning your sprites can be in any size, any place on the spritesheet.

## Templates

You can control this library with regular lua, but if you want to separate your animation code from your classes, you can stick all the 
animation configuration in YAML files. Here's a small example:

```yaml
spritesheet: "sprites.png"
height: 22
width: 11
states:
  run:
    frames:
      - [ 0, 0, 22, 22 ]
      - [ 22, 0, 22, 22 ]
      - [ 44, 0, 22, 22 ]
      - [ 66, 0, 22, 22 ]
      - [ 88, 0, 22, 22 ]
      - [ 110, 0, 22, 22 ]
      - [ 132, 0, 22, 22 ]
    fps: 16
    offset: [8, 0]
initial: run
```

To understand it better, I'll go through each property and what it does.
#### spritesheet
This is pretty obvious. Just give it the path of the spritesheet to load

#### height and width
These are the sizes of your bounding box (NOT the size of your viewport)

#### initial
Tells the library would your starting animation should be. Very important, otherwise it gets confused.

#### states
States is where you create all your different character states, ie: idle, run, jump, fall, etc. You'll need to nest the following under it.

#### frames
Takes the x position, y position, viewport width, viewport height, just like a regular quad. The library goes through each of these and creates a new quad for each frame.

#### fps
This is how fast the animation is, 1 being the slowest. If you set it to `0`, then it simply won't bother updating it. Useful if you only have one frame and no rotation.

#### offset
Takes two parameters, an x and y. The xoffset is basically the width between the start of your viewport and the bounding box, the y is the height. Usually you'll need to play around with this to get it right. If your collision library can draw the bounding box for you, it would be very helpful.

#### callback
When an animation completes its last frame, you can give it a callback. This will call a method on the object you gave the library when creating the instance. Useful for doing things like changing to a new state, or performing an action after the animation.

#### rotate
This causes the sprite to spin clockwise in 30 degree increments, or counter-clockwise if `flip` is true.

## Creating an instance

```lua
local Anim = require "lib.Animation" -- (or whever you put it)

function MySexyObject:new(...)
  MySexyObject.super.new(self, ...)

  -- first argument can be the path to a template, or a love2d Image
  self.spr = Anim("tmpl/player.yml", self)
end
```

To actually update the sprites and draw them is very simple also.

```lua
function MySexyObject:update(dt)
  MySexyObject.super.update(self, dt)

  self.spr:update(dt)
end

function MySexyObject:draw()
  MySexyObject.super.draw(self)

  self.spr:draw()
end
```

And that's it!

## Flipping sprites

No animation library is complete without being able to flip sprites. You can mirror them (flip horizontally) by setting `flip` to true.

```lua
self.spr.flip = true -- flip the sprite
self.spr.flip = false -- return it to normal
```

## Changing states

To swap states, for example going from 'idle' to 'run', you simply invoke `set_state`

```lua
self.spr:set_state("omgimrunninglol")
```

## But I hate YAML...

No judgement here. It's fine, if you don't want to use YAML, you can still do stuff in Lua-land like so

```lua
-- instead of passing a path as the first argument, you pass it a love image (No not that kind of love image ;))
local spritesheet = love.graphics.newImage("SexySprites.png")
self.spr = Anim(spritesheet, self)

self.spr:add_animation("running", {
  { 0, 0, 64, 64 },
}, 10, { x = 0, y = 0 }, function() self:do_this() end)

-- don't forget to set the initial state (make sure it exists, unlike this example)
self.spr:set_state("idle")
```

Yeah, this is why I prefer the YAML approach. First argument to `add_animation` is your state name, second argument is a list of tables containing the quad data. Third argument is the fps, forth argument is a table with the x and y offsets, and finally the fifth argument is the callback. Of course, you don't have to pass a callback if you don't want to.