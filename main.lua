love.graphics.setDefaultFilter("nearest", "nearest")

local Class = require "lib.Class"
local Anim = require "lib.Animation"

--[[ player class ]]
local Player = Class:extends()

function Player:new(x, y)
	self.x, self.y = x, y
	self.spr = Anim("player.yml", self)
end

function Player:update(dt)
	self.spr:update(dt)
end

function Player:draw()
	self.spr:draw()
end

function Player:change_to_attack(a) a:set_state("attack") end
function Player:change_to_spin(a) a:set_state("spin") end

--[[ end player class ]]

local ply = Player(64, 64)
local ply2 = Player(64, 96)

ply2.spr.flip = true -- mirror image
ply2.spr:set_state("run")

function love.update(dt)
	ply.spr:update(dt)
	ply2.spr:update(dt)
end

function love.draw()
	love.graphics.clear(0.1, 0.1, 0.1, 1)
	love.graphics.scale(4)
	ply.spr:draw()
	ply2.spr:draw()
end

