local Class = require "lib.Class"

local State = Class:extends()

function State:new(sheet, name, frames, fps, offsets, cb)
	self.name = name
	self.pos = 1
    self.offset = offsets or { x = 0, y = 0 }
    self.cb = cb or false
    self.t = 1 / fps
    self.fps = fps
    self.quads = {}
    self.rotate = false
    self.paused = false
    self.rotate_deg = 0

    for i = 1, #frames do
        self.quads[i] = love.graphics.newQuad(frames[i][1], frames[i][2], frames[i][3], frames[i][4],
            sheet:getDimensions())
    end
end

return State